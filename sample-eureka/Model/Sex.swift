//
//  Sex.swift
//  sample-eureka
//
//  Created by Tiago Maia Lopes on 07/01/20.
//  Copyright © 2020 Tiago Maia Lopes. All rights reserved.
//

import Foundation

enum Sex: CustomStringConvertible {
    case male
    case female

    var description: String {
        switch self {
        case .male:
            return "Masculino"

        case .female:
            return "Feminino"
        }
    }
}
