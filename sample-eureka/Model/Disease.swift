//
//  Disease.swift
//  sample-eureka
//
//  Created by Tiago Maia Lopes on 08/01/20.
//  Copyright © 2020 Tiago Maia Lopes. All rights reserved.
//

import Foundation

struct Disease {
    let name: String
}

extension Disease: CustomStringConvertible {
    var description: String { name }
}

extension Disease: Equatable {
    static func == (lhs: Self, rhs: Self) -> Bool { lhs.name == rhs.name }
}
