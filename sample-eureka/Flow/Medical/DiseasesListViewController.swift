//
//  DiseasesListViewController.swift
//  sample-eureka
//
//  Created by Tiago Maia Lopes on 08/01/20.
//  Copyright © 2020 Tiago Maia Lopes. All rights reserved.
//

import UIKit
import Eureka

class DiseasesListViewController: BaseFormViewController, TypedRowControllerType {

    // MARK: Properties

    private let diseases = [
        "Meduloepitelioma orbital",
        "Megacalicose congénita",
        "Megacalicose congénita bilateral",
        "Megacalicose congénita unilateral",
        "Megacisterna magna",
        "Megacistis - microcólon - hipoperistaltismo intestinal - hidronefrose",
        "Megaduodeno e/ou megabexiga",
        "Megalencefalia"
        ].map({ Disease(name: $0) })

    // MARK: TypedRowControllerType Implementation

    typealias RowValue = Disease
    var row: RowOf<Disease>!
    var onDismissCallback: ((UIViewController) -> Void)?

    // MARK: Forms

    override func setupForm() {
        super.setupForm()

        let listSection = SelectableSection<DiseaseRow>(
            "What disease do you have?",
            selectionType: SelectionType.singleSelection(enableDeselection: false)
        )
        for disease in diseases {
            listSection <<< LabelRow(disease.name) { [unowned self] row in
                row.value = disease.name
                row.cell.accessoryType = self.row.value == disease ? .checkmark : .none
                row.onCellSelection { [unowned self] _, _ in
                    self.row.value = disease
                    self.dismiss(animated: true)
                }
            }
        }

        form +++ listSection
    }
}
