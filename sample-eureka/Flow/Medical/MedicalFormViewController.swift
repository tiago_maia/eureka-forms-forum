//
//  MedicalFormViewController.swift
//  sample-eureka
//
//  Created by Tiago Maia Lopes on 07/01/20.
//  Copyright © 2020 Tiago Maia Lopes. All rights reserved.
//

import UIKit
import Eureka

class MedicalFormViewController: BaseFormViewController {

    // MARK: Tags

    private enum Tags {
        static let name = "name"
        static let sex = "sex"
        static let birthday = "birthday"

        static let complete = "complete"
    }

    // MARK: Forms

    override func setupForm() {
        super.setupForm()

        form
            +++ Section("Informações pessoais")
            <<< TextRow(Tags.name) {
                $0.title = "Nome completo"

                $0.add(rule: RuleRequired())
                $0.validationOptions = .validatesAlways
            }
            <<< SegmentedRow<Sex>(Tags.sex) {
                $0.title = "Sexo"
                $0.options = [.male, .female]

                $0.add(rule: RuleRequired())
                $0.validationOptions = .validatesAlways
            }
            <<< DateInlineRow(Tags.birthday) {
                $0.title = "Data de nascimento"

                $0.add(rule: RuleRequired())
                $0.validationOptions = .validatesAlways
            }

            +++ MultivaluedSection(
                multivaluedOptions: [.Reorder, .Insert, .Delete],
                header: "Doenças"
            ) {
//                $0.hidden = Condition(
//                    stringLiteral: "$\(Tags.name) == NULL OR $\(Tags.sex) == NULL OR $\(Tags.birthday) == NULL"
//                )
                $0.addButtonProvider = { section in
                    ButtonRow(){
                        $0.title = "Select a disease"
                    }
                }
                $0.multivaluedRowToInsertAt = { index in
                    DiseaseRow() {
                        $0.title = "Disease name"
                    }
                }
            }

            +++ Section()
            <<< ButtonRow(Tags.complete) {
                $0.title = "Enviar"
                $0.disabled = Condition.function([Tags.name, Tags.sex, Tags.birthday], { form in
                    let errors = form.validate()
                    return !errors.isEmpty
                })
        }
    }
}
