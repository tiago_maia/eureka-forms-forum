//
//  BaseFormViewController.swift
//  sample-eureka
//
//  Created by Tiago Maia Lopes on 07/01/20.
//  Copyright © 2020 Tiago Maia Lopes. All rights reserved.
//

import UIKit
import Eureka

class BaseFormViewController: FormViewController {

    // MARK: Lifecycle

    override func viewDidLoad() {
        super.viewDidLoad()
        setupForm()
    }

    // MARK: Forms

    func setupForm() {}
}
