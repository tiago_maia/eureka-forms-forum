//
//  DiseaseRow.swift
//  sample-eureka
//
//  Created by Tiago Maia Lopes on 08/01/20.
//  Copyright © 2020 Tiago Maia Lopes. All rights reserved.
//

import UIKit
import Eureka

class DiseaseCell: Cell<Disease>, CellType {

    // MARK: Lifecycle

    override func setup() {
        super.setup()
        accessoryType = .disclosureIndicator
    }

    override func update() {
        super.update()

        if let title = row.title, row.value == nil {
            textLabel?.text = title
        } else {
            textLabel?.text = row.value?.description
        }
    }
}

final class DiseaseRow: Row<DiseaseCell>, PresenterRowType {

    // MARK: Lifecycle

    override func customDidSelect() {
        super.customDidSelect()

        debugPrint("was selected")

        guard let controller = presentationMode?.makeController() else {
            assertionFailure("The diseases controller must be presented.")
            return
        }
        controller.row = self
        controller.title = "Doenças"
        presentationMode?.present(controller, row: self, presentingController: self.cell.formViewController()!)
    }

    // MARK: PresenterRowType Implementation

    var presentationMode: PresentationMode<DiseasesListViewController>? = .presentModally(
        controllerProvider: ControllerProvider<DiseasesListViewController>.callback(builder: { DiseasesListViewController() }),
        onDismiss: { controller in
            controller.dismiss(animated: true)
    })

    var onPresentCallback: ((FormViewController, DiseasesListViewController) -> Void)?

    typealias PresentedControllerType = DiseasesListViewController
}

extension DiseaseRow: SelectableRowType {

    // MARK: PresenterRowType Implementation

    var selectableValue: Disease? {
        get {
            value
        }
        set(newValue) {
            value = newValue
        }
    }
}
